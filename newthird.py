from dolfin import *
import mpi4py
import numpy as np
import time
import sys

'''
New third formulation with different order, i.e., 
solve porosity advection first and solve velocity equation
'''

set_log_active(False)

comm = mpi_comm_world()
my_rank = MPI.rank(comm)

exp_num = None
exp_num = sys.argv[-1]
if len(exp_num) > 2:
    exp_num = None

if my_rank == 0:
    print 'exp_num = ', exp_num

x0, x1 = 0., .1
y0, y1 = 0., .2

### change pg + (1-phi)*pg in another.py to pt (total pressure)

parameters['ghost_mode'] = 'shared_facet'   # for MPI with DG restriction operator

mesh = RectangleMesh(Point(x0, y0), Point(x1, y1), 64, 128, 'crossed')

Vm = VectorFunctionSpace(mesh, 'CG', 2)
Vd = FunctionSpace(mesh, 'RT', 1)
Pt = FunctionSpace(mesh, 'CG', 1)
Pg = FunctionSpace(mesh, 'DG', 0)

Phi = FunctionSpace(mesh, 'DG', 0)

ME = MixedElement([Vm.ufl_element(), Vd.ufl_element(), Pt.ufl_element(), Pg.ufl_element() ])
Vh = FunctionSpace(mesh, ME)

fct = Function(Vh)
vm, vd, pt, pg = split(fct) 
wm, wd, qt, qg = TestFunctions(Vh)

n = FacetNormal(mesh)
h = CellSize(mesh)

class Side(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], x1) or near(x[0], x0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], y1)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], y0)

zerovec = Constant((0., 0.))
vm0 = Expression(('0.', '100*x[0]*(.1-x[0])'), t = 0., degree = 2)
vd0 = vm0
phi0 = Expression('.2', t = 0., degree = 1)
pg0 = Constant(0.)
rhom = Constant(1.)
rhog = Constant(.2)
gravity = Constant(1.)
gradz = Constant((0., 1.))
C = Constant(1.)    # mu_g / K_phi
mu_m = Constant(1.)

### boundary conditions
side = Side()
top = Top()
bottom = Bottom()

meshdata = MeshFunction('size_t', mesh, 1)
meshdata.set_all(0)

top.mark(meshdata, 2)
side.mark(meshdata, 1)
bottom.mark(meshdata, 0)

ds = ds(subdomain_data = meshdata)
dS = dS(subdomain_data = meshdata)

vm_bottom = DirichletBC(Vh.sub(0), vm0, meshdata, 0)
vm_side = DirichletBC(Vh.sub(0), zerovec, meshdata, 1)

vd_bottom = DirichletBC(Vh.sub(1), vm0, meshdata, 0)
vd_side = DirichletBC(Vh.sub(1), zerovec, meshdata, 1)

phi_bottom = DirichletBC(Phi, phi0, meshdata, 0)

if my_rank == 0:
    File('meshdata.pvd') << meshdata

### define velocity-pressure bilinear form

def VelocityForm(state, test, phi):    
    vm, vd, pt, pg = split(state)
    wm, wd, qt, qg = split(test)
    sigma = (Constant(1.) - phi)*mu_m*(grad(vm) + grad(vm).T - Constant(2./3.)*div(vm)*Identity(2)) - pt*Identity(2)
    rho_bar = (Constant(1.) - phi)*rhom + phi*rhog
   
    form =  inner(sigma, grad(wm))*dx + rho_bar*gravity*dot(gradz, wm)*dx \
            + (C/(phi*phi))*dot(vd, wd)*dx + pg*div(wd)*dx - rhog*gravity*dot(gradz, wd)*dx - pg0*phi*dot(wd, n)*ds(2) \
            + div(vd)*qg*dx + (phi/((Constant(1.) - phi)*mu_m))*(pt - pg)*qg*dx \
            - div(vm)*qt*dx - (phi/((Constant(1.) - phi)*mu_m))*(pt - pg)*qt*dx
    return form
   

vm_init = vm0
vd_init = vd0
pt_init = Constant(0.1)
pg_init = Constant(0.1)
phi_init = Constant(.3)


state = Function(Vh)

vm, vd, pt, pg = TrialFunctions(Vh)
a_init = dot(vm, wm)*dx + dot(vd, wd)*dx + (pt*qt + pg*qg)*dx
L_init = (dot(vm_init, wm) + dot(vd_init, wd) + pt_init*qt + pg_init*qg )*dx

bcs = [vm_bottom, vm_side, vd_bottom, vd_side]

A = assemble(a_init)
b = assemble(L_init)
solve(a_init == L_init, state, bcs=bcs)
vm_old, vd_old, pt_old, pg_old = state.split(deepcopy = True)

phi_old = interpolate(phi_init, Phi)
vm_func = Function(Vm, name = 'vm')
vd_func = Function(Vd, name = 'vd')
pg_func = Function(Pg, name = 'pg')
pt_func = Function(Pt, name = 'pt')

phi = TrialFunction(Phi)
psi = TestFunction(Phi)
phi_sol = Function(Phi)
sol = Function(Vh)

if exp_num:
    phi_file = File('results/phi'+exp_num+'.pvd')
    vm_file = File('results/vm'+exp_num+'.pvd')
    vd_file = File('results/vd'+exp_num+'.pvd')
    pg_file = File('results/pg'+exp_num+'.pvd')
    pt_file = File('results/pt'+exp_num+'.pvd')
else:
    phi_file = File('results/phi.pvd')
    vm_file = File('results/vm.pvd')
    vd_file = File('results/vd.pvd')
    pg_file = File('results/pg.pvd')
    pt_file = File('results/pt.pvd')

phi_old.rename('phi', phi_old.name())
vm_func.assign(vm_old)
vd_func.assign(vd_old)
pt_func.assign(pt_old)
pg_func.assign(pg_old)

phi_file << (phi_old, 0.)
vm_file << (vm_old, 0.)
vd_file << (vd_old, 0.)
pg_file << (pg_old, 0.)
pt_file << (pt_old, 0.)

dt = .5*mesh.hmin()
t = 0.
count = 0


Fsolver = None
Phisolver = None

### bilinear form for degree >= 1
phi_eq = Constant(1./dt)*(phi - phi_old)*psi*dx + div(vm_old)*phi_old*psi*dx \
            - Min(dot(vm_old, n), 0.)*phi_old*psi*ds \
            - dot(vm_old, n('+'))*(phi_old('+') - phi_old('-'))*avg(psi)*dS \
            + Constant(.5)*sqrt(abs(dot(vm_old, n('+'))*dot(vm_old, n('+'))))*jump(phi_old)*jump(psi)*dS \
            - div(vd_old)*psi*dx \
            + Constant(1.)*(phi - phi0)*psi*ds(0)

if Phi.ufl_element().degree() > 0:
    phi_eq += dot(vm_old, grad(phi_old))*psi*dx 

#phi_eq = Constant(1./dt)*(phi - phi_old)*psi*dx + div(vm_old)*phi_old*psi*dx + dot(vm_old, grad(phi_old))*psi*dx \
#            - Min(dot(vm_old, n), 0.)*phi_old*psi*ds \
#            - dot(vm_old, n('+'))*(phi_old('+') - phi_old('-'))*avg(psi)*dS \
#            + Constant(.5)*sqrt(abs(dot(vm_old, n('+'))*dot(vm_old, n('+'))))*jump(phi_old)*jump(psi)*dS \
#            - div(vd_old)*psi*dx \
#            + Constant(1.)*(phi - phi0)*psi*ds(0)


F = VelocityForm(TrialFunction(Vh), TestFunction(Vh), phi_old)


start_time = time.time()
vm_old = interpolate(Constant((.05, .1)), Vm)
if my_rank == 0:
    print 'Start time stepping'
while t < 100*dt:
    t += dt
    count += 1

    ### solve porosity equation
    #phi0.t = t
    #lhs_phi = lhs(phi_eq)
    #rhs_phi = rhs(phi_eq)
    #solve(lhs_phi == rhs_phi, phi_sol, solver_parameters=dict(linear_solver='cg'))

    ### LU solve approach
    if Phisolver == None:
        Phisolver = PETScLUSolver(mesh.mpi_comm(), 'superlu_dist')
        A = assemble(lhs(phi_eq))
        Phisolver.set_operator(A)
    Lphi = assemble(rhs(phi_eq))
    Phisolver.solve(phi_sol.vector(), Lphi)
    phi_old.vector().zero()
    phi_old.vector().axpy(1., phi_sol.vector())


    ### solve velocity equation

    lhs_form = lhs(F)
    rhs_form = rhs(F)

    vm0.t = t
    solve(lhs_form == rhs_form, sol, bcs = bcs, solver_parameters=dict(linear_solver='gmres', preconditioner='hypre_amg') ) 

    ### LU solve approach
    #if Fsolver ==None:
    #    Fsolver = PETScLUSolver(mesh.mpi_comm(), 'superlu_dist')
    #A = assemble(lhs(F))
    #for bc in bcs:
    #    bc.t = t
    #    bc.apply(A)
    #Fsolver.set_operator(A)
    #L = assemble(rhs(F))
    #for bc in bcs:
    #    bc.t = t
    #    bc.apply(L)
    #Fsolver.solve(sol.vector(), L)


    vm_old, vd_old, pt_old, pg_old = sol.split(deepcopy = True)
    vm_func.vector().zero()
    vm_func.vector().axpy(1., vm_old.vector())
    vd_func.vector().zero()
    vd_func.vector().axpy(1., vd_old.vector())
    pg_func.vector().zero()
    pg_func.vector().axpy(1., pg_old.vector())
    pt_func.vector().zero()
    pt_func.vector().axpy(1., pt_old.vector())

    if count%20 == 0:
        vm_file << (vm_func, t)
        #vd_file << (vd_func, t)
        #pg_file << (pg_func, t)
        #pt_file << (pt_func, t)
        phi_file << (phi_old, t)

    if count%100 == 0:
        if my_rank == 0:
            print count
if my_rank == 0:
    print 'Elapsed time = {0}'.format(time.time() - start_time)

