from dolfin import *
import numpy as np

x0, x1 = 0., 1.
y0, y1 = 0., 1.


#mesh = UnitSquareMesh(4,4)
mesh = RectangleMesh(Point(x0, y0), Point(x1, y1), 4,4)

Vm = VectorFunctionSpace(mesh, 'CG', 2)
Vd = FunctionSpace(mesh, 'RT', 1)
#Vd = VectorFunctionSpace(mesh, 'CG', 2)
Pd = FunctionSpace(mesh, 'CG', 1)
Pg = FunctionSpace(mesh, 'DG', 0)
#Pg = FunctionSpace(mesh, 'CG', 1)

Phi = FunctionSpace(mesh, 'DG', 1)

ME = MixedElement([Vm.ufl_element(), Vd.ufl_element(), Pd.ufl_element(), Pg.ufl_element() ])
Vh = FunctionSpace(mesh, ME)

fct = Function(Vh)
vm, vd, pd, pg = split(fct) #TrialFunctions(Vh)
wm, wd, qd, qg = TestFunctions(Vh)


n = FacetNormal(mesh)

class Side(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], x1) or near(x[0], x0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], y1)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], y0)

zerovec = Constant((0., 0.))
vm0 = Expression(('0.', 'x[0]*(1.-x[0])'), degree = 2)
vd0 = vm0
phi0 = Constant(0.1)
pg0 = Constant(0.)
rhom = Constant(1.)
rhog = Constant(.2)
gravity = Constant(1.)
gradz = Constant((0., 1.))
C = Constant(1.)    # mu_g / K_phi
mu_m = Constant(1.)

side = Side()
top = Top()
bottom = Bottom()

meshdata = MeshFunction('size_t', mesh, 1)
meshdata.set_all(0)

top.mark(meshdata, 2)
side.mark(meshdata, 1)
bottom.mark(meshdata, 0)

vm_bottom = DirichletBC(Vh.sub(0), vm0, meshdata, 0)
vm_side = DirichletBC(Vh.sub(0), zerovec, meshdata, 1)

vd_bottom = DirichletBC(Vh.sub(1), vm0, meshdata, 0)
vd_side = DirichletBC(Vh.sub(1), zerovec, meshdata, 1)

phi_bottom = DirichletBC(Phi, phi0, meshdata, 0)

def NonlinearForm(state, test, phi):    
    vm, vd, pd, pg = split(state)
    wm, wd, qd, qg = split(test)
    sigma = (Constant(1.) - phi)*mu_m*(grad(vm) + grad(vm).T - Constant(2./3.)*div(vm)*Identity(2)) + ((Constant(1.)- phi)*pd + pg)*Identity(2)
    rho_bar = (Constant(1.) - phi)*rhom + phi*rhog
   
    form = div(vm - phi*vd)*qg*dx \
            - pg*dot(wd, n)*ds(2) + pg0*dot(wd, n)*ds(2) + pg*div(wd)*dx - rhog*gravity*dot(gradz, wd)*dx + (C/phi)*dot(vd, wd)*dx \
            - dot(sigma*n, wm)*ds(2) + inner(sigma, grad(wm))*dx + rho_bar*gravity*dot(gradz, wm)*dx - ((Constant(1.)- phi)*pd + pg)*div(wm)*dx \
            + (mu_m*div(vm) + phi*pd)*qd*dx
    return form
   

vm_init = vm0
vd_init = vd0
pd_init = Constant(0.)
pg_init = Constant(0.)
phi_init = Constant(0.1)


state = Function(Vh)

vm, vd, pd, pg = TrialFunctions(Vh)
a_init = dot(vm, wm)*dx + dot(vd, wd)*dx + (pd*qd + pg*qg)*dx
L_init = (dot(vm_init, wm) + dot(vd_init, wd) + pd_init*qd + pg_init*qg )*dx

bcs = [vm_bottom, vm_side, vd_bottom, vd_side]

A = assemble(a_init)
b = assemble(L_init)
solve(a_init == L_init, state, bcs=bcs)

phi_old = interpolate(Constant(0.5), Phi)
phi = TrialFunction(Phi)
psi = TestFunction(Phi)
phi_sol = Function(Phi)
sol = Function(Vh)

dt = 0.1*mesh.hmin()
t = 0.
while t < 100*dt:
    t += dt
    F = NonlinearForm(state, TestFunction(Vh), phi_old)
    dF = derivative(F, state, TrialFunction(Vh))

    solve(dF == rhs(F), sol, bcs = bcs)
    print 'sol norm', sol.vector().inner(sol.vector())
    vm_old, vd_old, pd_old, pg_old = sol.split(deepcopy = True)

    phi_eq = Constant(1./dt)*(phi - phi_old)*psi*dx - div(vm_old)*(Constant(1.) - phi)*psi*dx + dot(vm_old, grad(phi))*psi*dx \
                - Min(dot(vm_old, n), 0.)*phi*psi*ds(2) \
                - dot(vm_old, n)*phi0*psi*ds(0) \
                - dot(vm_old, n('+'))*jump(phi)*avg(psi)*dS \
                + Constant(0.5)*dot(vm_old, n('+'))*jump(phi)*jump(psi)*dS

    solve(lhs(phi_eq) == rhs(phi_eq), phi_sol)
    phi_old.assign(phi_sol)
    print 'phi norm', phi_sol.vector().inner(phi_sol.vector())

    #vm_file = File('vm.pvd')
    #vm_file << vm_old
    #vd_file = File('vd.pvd')
    #vd_file << vd_old
    #pg_file = File('pg.pvd')
    #pg_file << pg_old



